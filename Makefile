.PHONY: local clean

HOSTS   ?= $(shell hostname --short),
TAGS    ?= all

local: playbook.yml
	ansible-playbook --connection=local --vault-password-file=bin/vault-password --inventory=$(HOSTS) --tags=$(TAGS) $<

clean:
	$(RM) playbook.retry
